db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
		]);

// [SECTION] MongoDB Aggregation
/*
	-used to generate, manipulate and perform operations to create filtered results that can help us to analyze the data
*/


// Using the Aggregate Method:
/*
	- the "$match" is used to pass the docs that meet the specified conditions to the next stage/aggregation process.
	Syntax:
		{$match {field: value}}

	- the "$group" is used to group elements together and field-value pairs of the data from the grouped elements
	Syntax:
		{$group: {_id: "value", field result: "valueResult"}}

	- using both $match and $group along with aggregation will find for product are on sale and will group the total amount of stock for all suppliers found.

	- the "$" symbol will refer to a field name that is available in the docs that are being aggregated on.
*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
	]);


// Field Projection with Aggregation

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$project: {_id: 0}}
	]);


// Sorting aggregated results
/*
	= the "$sort" can be used to change the order of the new aggregated result
	Syntax:
		{sort: {field: 1/-1}}
		1 -> lowest to highest
		-1 -> highest to lowest
*/

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
		{$sort: {total: 1}}
	]);


// Aggregating results based on array fields ($UNWIND)
/*
	- the "$unwind" deconstructs an array fields from a collection/field with an array value to output a result
	Syntax:
		{$unwind: field}
*/

db.fruits.aggregate(
	[
		{$unwind: "$origin"},
		{$group: {_id: "$origin", fruits: {$sum: 1}}}
	]

)


// [SECTION] Other Aggregate Stages

// count all yellow fruits
db.fruits.aggregate([
	{$match: {color: "Yellow"}},
	{$count: "Yellow Fruits"}
])


// $avg
// getting the average value of stock

db.fruits.aggregate(
	[
		{$match: {color: "Yellow"}},
		{$group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
	]
)

// $min & $max
db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}},
			{$group: {_id: "$color", yellow_fruits_stock: {$min: "$stock"}}}
		]
	)


db.fruits.aggregate(
		[
			{$match: {color: "Yellow"}},
			{$group: {_id: "$color", yellow_fruits_stock: {$max: "$stock"}}}
		]
	)







